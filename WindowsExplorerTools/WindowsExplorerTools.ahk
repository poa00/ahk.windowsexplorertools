; AHK v2
#Warn all, StdOut
#SingleInstance Force
SetWorkingDir A_ScriptDir
global A := 'A'

#Include Version.ahk

#Include ..
#Include AskMenuList\AskMenuList.ahk
#Include WinClip\WinClip.ahk
#Include WET_lib\findPath.ahk
#Include WET_lib\bitmapToFile.ahk

#Include WET_lib\launch.ahk

#Include WET_lib\winRButton.ahk
#RButton::WinRButton()

#Include WET_lib\winEnter.ahk
*#Enter::WinEnter()

#Include WET_lib\winSlash.ahk
#/::WinSlash()
#NumpadDiv::WinSlash()
#VKBF::WinSlash()

#Include WET_lib\winF2.ahk
#HotIf WinActive("ahk_exe explorer.exe")
#F2::WinF2()


#Include WET_lib/ctrlV.ahk
#HotIf not WinActive("ahk_exe lync.exe")
*^v::CtrlV()

#!Up::
{
  WinSetAlwaysOnTop -1, A

  ExStyle := WinGetExStyle(A)
  activeTitle := WinGetTitle(A)
  
  if (ExStyle & 0x8)  ; 0x8 is WS_EX_TOPMOST.
    toolTipAutoClose '"' activeTitle '" is always on top', 1500
  else
    toolTipAutoClose '"' activeTitle '" is not always on top', 1500
}


#Include WET_lib\virtualBox.ahk
workWithVirtualBox()

StrJoin(arrStr, midle, begin := "", end := "")
{
  if arrStr is string
    return begin arrStr end

  if arrStr.Length = 0
    return begin end

  res := begin arrStr[1]
  
  loop arrStr.Length-1
    res .= midle arrStr[A_Index + 1]

  return res end
}


pushIfUnique(&val, &arr)
{
  for value in arr
    if value = val
      return	

  arr.Push val
}

iniLoad(strValName, defaultVal)
{
  return IniRead('WET.ini', 'save', strValName, defaultVal) 
}

iniSave(strValName, Val)
{
  if Trim(Val) != Val
    val := '"' val '"'

  IniWrite Val, 'WET.ini', 'save', strValName
}

toolTipAutoClose(strMsg, time_ms := 3000)
{
  if(time_ms > 0)
    time_ms := -time_ms

  ToolTip strMsg 
  SetTimer () => ToolTip(), time_ms
}

; setPSready(str)
; {
;   newstr := StrReplace(str, "(", "``(")
;   newstr := StrReplace(newstr, ")", "``)")
;   newstr := StrReplace(newstr, " ", "`` ")
;   return newstr
; }


