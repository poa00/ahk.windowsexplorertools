
WinSlash()
{
  WClip.iCopy()
  str := WClip.iGetText()
  WClip.iClear()

  if !str
    return
  
  isSlash     := InStr(str, "/")
  isBkSlash   := InStr(str, "\")
  DblBkSlash  := InStr(str, "\\")

  if !isSlash and !isBkSlash
    return
  
  lengthExtract := 30
  choices := []
  if isSlash 
  {
    toBkSlash := StrReplace(str, "/", "\")
    choices.push (["switch '/'=>'\'`t..." SubStr(toBkSlash, isSlash, lengthExtract) "...", toBkSlash])

    toDblBkSlash := StrReplace(str, "/", "\\")
    choices.push (["switch and escape '/'=>'\\'`t..." SubStr(toDblBkSlash, isSlash, lengthExtract) "...", toDblBkSlash])
  }

  if isBkSlash 
  {
    toSlash := StrReplace(str, "\", "/")
    choices.push (["switch '\'=>'/'`t..." SubStr(toSlash, isBkSlash, lengthExtract) "...", toSlash])
    
    choices.Push([])

    toEscape := StrReplace(str, '\', '\\')
    choices.push (["escape '\'=>'\\'`t..." SubStr(toEscape, isBkSlash, lengthExtract) "...", toEscape])
    if DblBkSlash
    {
      toUnEscape := StrReplace(str, '\\', '\')
      choices.push (["unescape '\\'=>'\'`t..." SubStr(toUnEscape, isBkSlash, lengthExtract) "...", toUnEscape])
    }
  }

  if choices.Length = 1
    str := choices[1][2]
  else
    str := askMenuList(choices)
 
  if str
    WClip.Paste(str)

  return
}

; choices.push [SubStr(toUnEscape, isBkSlash, 20) "`t'\\'to'\'", toUnEscape]
; PS //work/macro\/ExplorerTools_ahk/windowsexplorertools>