
WinRButton()
{
  local winPID
  local winDir

  MouseGetPos &x, &y, &winID
  winPID        := WinGetPID("ahk_id " WinID)
  ProcessName   := WinGetProcessName("ahk_id " WinID)
  ProcessPath   := WinGetProcessPath("ahk_id " WinID)
  winTitle      := WinGetTitle("ahk_id " WinID)

  SplitPath ProcessPath,, &winDir
  
  ExStyle := WinGetExStyle("ahk_id " WinID)
  if (ExStyle & 0x8)  ; 0x8 is WS_EX_TOPMOST.
    alwaysTopMsg := '>Always on Top`t(Win+Alt+Up)'
  else
    alwaysTopMsg := 'Always on Top`t(Win+Alt+Up)'


  chose := askMenuList([
    [alwaysTopMsg                           , 5],
    ,
    ['Open install folder (' winDir ')'     , 3],
    ,
    ['Copy ExePath (' ProcessPath ')'       , 4],
    ['Copy PID (' winPID ')'                , 2],
    ,
    ['Kill ' winTitle ' (' ProcessName ')'  , 1]
  ])

  if chose == 1
  {
    rep := MsgBox('Would you want to stop ' ProcessName ' ?', 'Kill process', "YesNo")
    if rep = "Yes"
      ProcessClose WinPID

  }
  Else if chose == 2
    A_Clipboard := winPID
  Else if chose == 3
    run "open " winDir
  Else if chose == 4
    A_Clipboard := ProcessPath
  Else if chose == 5
    WinSetAlwaysOnTop -1, "ahk_id " WinID
    
  Return
}
