; FIRST LAUNCH CASE
if !FileExist("WET.ini")
{
  Loop Files, ".\*"
  {
    if A_LoopFileName != A_ScriptName
    {
      MsgBox "ERROR :`nWindows Explorer Tools must be inside an empty folder for the first launch", "First Launch", "Iconx"
      return
    }
  }
  FileAppend "[cfg]", "WET.ini"
}


; SET ADMIN MODE IF NECESSARY
global forceAdmin := IniRead('WET.ini', 'cfg', 'forceAdmin', false)
if forceAdmin and !A_IsAdmin
{
    #include ..\ForceAdmin\ForceAdmin.ahk
}


; OLD AUTOUDPATE TOOL
;#Include %A_ScriptDir%\..\autoUpdate\checkNewVersion.ahk
; version := 7
;URLExeUpdate = https://s3.flexible-datastore.orange-business.com/di-diod-ahk-update-fe-buck/ExplorateurTools/ExplorateurTools.exe
;loadNewVersion(version, URLExeUpdate, A_Args)



; INSTALL SHORTCUT FOR START-UP AND WINDOWS MENU
shortCut := StrReplace(A_ScriptName, ".exe", ".lnk")
shortCut := StrReplace(shortCut, ".ahk", ".lnk")

if not DirExist(A_Programs "\WindowsExplorerTools")
  DirCreate A_Programs "\WindowsExplorerTools"

FileDelete A_Startup  "\WindowsExplorerTools*.lnk"
FileDelete A_Programs "\WindowsExplorerTools\WindowsExplorerTools*.lnk"
; FileDelete, %A_Programs%\WindowsExplorerTools\Uninstall*.lnk

FileCreateShortcut  A_ScriptFullPath, A_Startup "\" shortCut, A_ScriptDir
FileCreateShortcut  A_ScriptFullPath, A_Programs "\WindowsExplorerTools\" shortCut, A_ScriptDir
; FileCreateShortcut  %A_ScriptFullPath%, %A_Programs%\Uninstall\%shortCut%, %A_ScriptDir%, Uninstall


global WClip := WinClip() 

; Create context menu
{
  A_TrayMenu.Add  ; Creates a separator line.
  updateMenu := Menu()
  updateMenu.Add "Open release page", CheckNewVersion
  updateMenu.Add "Download last release", DownloadRelease
  updateMenu.Add
  updateMenu.Add "Download last experimental build", DownloadExperimental


  A_TrayMenu.Add "Check Version (" WETVersion ")", updateMenu    ; Creates a new menu item.
  A_TrayMenu.Add "Administrator", ToggleForceAdmin  ; Creates a new menu item.
  if A_IsAdmin and forceAdmin
    A_TrayMenu.Check("Administrator")  ; Creates a new menu item.

  A_TrayMenu.Add  ; Creates a separator line.

  helpMenu := Menu()
  helpMenu.Add "Help - En", HelpEn
  helpMenu.Add "Help - Fr", HelpFr

  A_TrayMenu.Add "Help", helpMenu  ; Creates a new menu item.
}


CheckNewVersion(*)
{
  run "https://gitlab.com/nitrateag/windowsexplorertools/-/releases"
}

DownloadRelease(*)
{
  DownloadLastBuild(false)
}
DownloadExperimental(*)
{
  DownloadLastBuild(true)
}

ToggleForceAdmin(*)
{
  global forceAdmin := ! IniRead("WET.ini", "cfg", "forceAdmin", 0)
  IniWrite forceAdmin, "WET.ini", "cfg", "forceAdmin"
  recallArgs := ""
  for , argument in A_Args
    recallArgs .= ' "' argument '"'
  Run '"' A_ScriptFullPath '"' recallArgs
}

HelpFr(*)
{   
  Run "https://gitlab.com/nitrateag/windowsexplorertools/-/blob/main/WindowsExplorerTools/Help/HelpFr.md"
}
HelpEn(*)
{   
  Run "https://gitlab.com/nitrateag/windowsexplorertools/-/blob/main/WindowsExplorerTools/Help/HelpEn.md"
}

iniSave "defaultPathDelimiter", A_Space


; -------------------------------------------------------------------
; -------------------------------------------------------------------
; -------------------------------------------------------------------
; -------------------------------------------------------------------

extractVersion(whrReponse)
{
  if RegExMatch(whrReponse, '"version":"(\d+\.\d+\.\d+)"', &matchVersion)
      return matchVersion[1]
  else
  {
    msgBox "Error : Cannot find last version available", "Error contacting GitLab", "Iconx"
    return "0.0.0"
  }
}

curl(url, methode := "GET")
{
  req := ComObject("Msxml2.XMLHTTP")
  req.open(methode, url, false)
  req.send()

  return req.responseText
}

DownloadLastBuild(experimentalBuild)
{
  lastRelVersion := extractVersion(curl("https://gitlab.com/api/v4/projects/36300439/packages?package_name=last_release", "GET"))
  package_name := "last_release"

  if experimentalBuild
  {
    lastExpVersion := extractVersion(curl("https://gitlab.com/api/v4/projects/36300439/packages?package_name=experimental", "GET"))
    
    if VerCompare(lastExpVersion, lastRelVersion) > 0
    {
      lastRelVersion := lastExpVersion
      package_name := "experimental"
    }
  }

  if VerCompare(lastRelVersion, WETVersion) > 0
  {
    run "https://gitlab.com/api/v4/projects/36300439/packages/generic/" package_name "/" lastRelVersion "/WindowsExplorerTools.exe"

    run A_ScriptDir

    downloadPath := A_MyDocuments "\..\Downloads"
    if(DirExist(downloadPath))
      run downloadPath
      
    ExitApp
  }
  else
    msgBox "You are already up to date"

}