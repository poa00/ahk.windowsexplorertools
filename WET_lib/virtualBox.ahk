
/*
if FileExist("\\Vboxsvr\c_drive")
    MsgBox 'we are virtal machine'
Else
    MsgBox 'we are Hist machine'
  */
  
; ------------------------- Conversion fileList to txt for virtualBox ---------------------
workWithVirtualBox()
{

  waitVirtualBox:
  ;MsgBox waitVirtual
  WinWait "ahk_exe VirtualBoxVM.exe"
  WinWaitActive "ahk_exe VirtualBoxVM.exe"

  if clipFiles := WClip.GetFiles()
  {
    clipFiles := RegExReplace(clipFiles, "\b([a-zA-Z]):[\/\\]", "\\Vboxsvr\$1_drive\")

    WClip.iSnap()
    
    
    ; arrFiles := StrSplit(StrReplace(clipFiles, "`r"), '`n')
    ; if arrFiles.Length > 1
    ; 	clipFiles := StrJoin(arrFiles, '" "', '"', '"')
    
    clipFiles := "¤¤¤FileList¤¤¤" . clipFiles
    WClip.SetText(clipFiles)
  }
  else if clipTxt := WClip.GetText()
  {
    WClip.SetText(RegExReplace(clipTxt, "\b([a-zA-Z]):[\/\\]", "\\Vboxsvr\$1_drive\"))
  }
    
  WinWaitNotActive "ahk_exe VirtualBoxVM.exe"
  
  clipTxt := WClip.GetText()
  if clipFiles and clipTxt == clipFiles 
        WClip.iRestore()
  Else if A_Clipboard
  {
    A_Clipboard := RegExReplace(A_Clipboard, "[\/\\][\/\\]Vboxsvr[\/\\]([a-zA-Z])_drive[\/\\]", "$1:\")
    if InStr(A_Clipboard, "¤¤¤FileList¤¤¤") = 1
    {
      WClip.SetFiles(StrReplace(A_Clipboard, "¤¤¤FileList¤¤¤",, true,, 1))
    }
  }
  
  goto waitVirtualBox
}
