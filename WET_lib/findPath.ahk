
; https://regex101.com/r/zWGLMP
global regExFindPath := "
(
  S)(?<opening>\b(?<montage>[a-zA-Z]:[\/\\])|[\/\\][\/\\](?<!http:\/\/)(?<!https:\/\/)(?>[?.][\/\\](?:[^\/\\<>:"|?\n\r ]+[\/\\])?(?&montage)?|(?!(?&montage)))|%\w+%[\/\\]?)(?:[^\/\\<>:"|?\n\r ,'][^\/\\<>:"|?\n\r]*(?<![ ,'])[\/\\])*(?:(?=[^\/\\<>:"'|?\n\r;, ])(?:(?:[^\/\\<>:"|?\n\r;, .](?: (?=[\w\-]))?(?:\*(?!= ))?(?!(?&montage)))+)?(?:\.\w+)*)|(?:'(?&opening)(?=.*'\W|.*'$)(?:[^\/\\<>:'"|?\n\r]+(?:'(?=\w))?[\/\\]?)*')|"(?&opening)(?=.*")(?:[^\/\\<>:"|?\n\r]+[\/\\]?)*"
)"

findPathsInStr_arr(str)
{
  local arrVal := []
  local match
  ;if StrLen(str) > 200000
    ;return 0

  begin := 1
  while(RegExMatch(str, regExFindPath, &match, begin))
  {
    path := Trim(match[0], " `t`"'`n")
    path := StrReplace(path, "/", "\")
    pushIfUnique &path, &arrVal

    begin := match.Pos + match.Len
    ; FoundPos := RegExMatch(str, regExFindPath, &match, begin)
  }

  Return arrVal
}

findValidPathsInStr_arr(str)
{
  local arrPath := findPathsInStr_arr(str)
  local arrValidPath := []

  for k, path in arrPath
    if FileExist(path)
      arrValidPath.Push path

  return arrValidPath
}

