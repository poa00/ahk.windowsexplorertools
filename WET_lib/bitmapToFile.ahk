

createFileImageFromClipboard(dir)
{
  lastExt := iniLoad('defaultImgExt', 'bmp')
  fileName := dir "\picture." lastExt

  askAgain := false
  ; reponse := InputBox('extention supported : `nBMP,DIB,RLE,JPG,JPEG,JPE,JFIF,GIF,TIF,TIFF,PNG', 'Create File from clipboard',, fileName)
  
  pToken := Gdip_Startup()

  allExt := Gdip_getAllEncoders().allExt
  reponse := FileSelect('S16', fileName,'Create File from clipboard', allExt)

  if not reponse
    return Gdip_Shutdown(pToken)
  filePath := reponse
  SplitPath filePath,,, &lastExt

  if not lastExt or not InStr(allExt, lastExt)
  {
    MsgBox 'Unable to save in "' lastExt '" format'
    return Gdip_Shutdown(pToken) 
  }

  iniSave('defaultImgExt', lastExt)
      ; Copy clipboard data to a variable
  pImage := 0
  DllCall("OpenClipboard", "Uint", 0)
  If	 DllCall("IsClipboardFormatAvailable", "Uint", 2) && (hBM:=DllCall("GetClipboardData", "Uint", 2))
     DllCall("gdiplus\GdipCreateBitmapFromHBITMAP", "Uint", hBM, "Uint", 0, "UintP", &pImage)
  DllCall("CloseClipboard")

  Gdip_SaveBitmapToFile(pImage, filePath)

  return Gdip_Shutdown(pToken) 
}

; Codec := {
;   buff        : Buffer(nSize)
;   allExt      : "*.jpg; *.jpeg; *.png; ...; *.bmp"
;   arrEncoder  : [{ptrCLSID, fileExt : "*.jpg;*.jpeg"}]
; }
Gdip_getAllEncoders()
{
  nCount := 0
  nSize := 0
  DllCall("gdiplus\GdipGetImageEncodersSize", "uint*", &nCount, "uint*", &nSize)
  ; VarSetCapacity(ci, nSize)
  Codec := {
    buff: Buffer(nSize),
    allExt: "",
    arrEncoder: []
  }

  DllCall("gdiplus\GdipGetImageEncoders", "uint", nCount, "uint", nSize, 'Ptr', codec.buff)
  if !(nCount && nSize)
    return -2
  
  offsetFileExtention := 32+3*A_PtrSize
  offsetCLSIDPtr := 0
  Loop nCount
  {
    idInBuff := (48+7*A_PtrSize)*(A_Index-1)
    sString := StrGet(NumGet(codec.buff, idInBuff + offsetFileExtention, "ptr"),, "UTF-16")
    Codec.arrEncoder.Push {ptrCLSID : Codec.buff.ptr + idInBuff, fileExt : sString}
    Codec.allExt .= sString ';'
  }
  ext := Trim(Codec.allExt, ';')
  Codec.allExt := StrReplace(ext, ';', '; ')

return Codec
/*
ImageEncoder *ci =
0               Clsid	            CLSID	Codec identifier.
16              FormatID	        GUID	File format identifier. GUIDs that identify various file formats (ImageFormatBMP, ImageFormatEMF, and the like) are defined in Gdiplusimaging.h.
32              CodecName         WCHAR *	Pointer to a null-terminated string that contains the codec name.
32 + ptrsize    DllName	          WCHAR *	Pointer to a null-terminated string that contains the path name of the DLL in which the codec resides. If the codec is not in a DLL, this pointer is NULL.
32 + 2*ptrsize  FormatDescription	WCHAR *	Pointer to a null-terminated string that contains the name of the file format used by the codec.
32 + 3*ptrsize FilenameExtension	WCHAR *	Pointer to a null-terminated string that contains all file-name extensions associated with the codec. The extensions are separated by semicolons.
MimeType	WCHAR *	Pointer to a null-terminated string that contains the mime type of the codec.
Flags	DWORD	Combination of flags from the ImageCodecFlags enumeration.
Version	DWORD	Integer that indicates the version of the codec.
SigCount	DWORD	Integer that indicates the number of signatures used by the file format associated with the codec.
SigSize	DWORD	Integer that indicates the number of bytes in each signature.
SigPattern	BYTE *	Pointer to an array of bytes that contains the pattern for each signature.
SigMask	BYTE *	Pointer to an array of bytes that contains the mask for each signature.

*/
}

Gdip_SaveBitmapToFile(pBitmap, sOutput, Quality := 75)
{
  Ptr := "Ptr"
  ; Ptr := A_PtrSize ? "UPtr" : "UInt"
  Extension := ""
  SplitPath sOutput,,, &Extension
  Extension := '*.' Extension
  
  codec := Gdip_getAllEncoders()
  
  pCodecCLSID := 0
  for encoder in codec.arrEncoder
  {
    if !InStr(encoder.fileExt, Extension)
      continue
    
    pCodecCLSID := encoder.ptrCLSID
    break
  }
  
  
  if !pCodecCLSID
    return -3

  p := 0
  if (Quality != 75)
  {
    Quality := (Quality < 0) ? 0 : (Quality > 100) ? 100 : Quality
    if InStr("|*.JPG|*.JPEG|*.JPE|*.JFIF|", '|' Extension '|')
    {
      DllCall("gdiplus\GdipGetEncoderParameterListSize", Ptr, pBitmap, Ptr, pCodecCLSID, "uint*", nSize)
      EncoderParameters := Buffer(nSize, 0)
      DllCall("gdiplus\GdipGetEncoderParameterList", Ptr, pBitmap, Ptr, pCodecCLSID, "uint", nSize, Ptr, EncoderParameters.Ptr)
      Loop  NumGet(EncoderParameters, "UInt")
      {
        elem := (24+(A_PtrSize ? A_PtrSize : 4))*(A_Index-1) + 4 + (pad := A_PtrSize = 8 ? 4 : 0)
        if (NumGet(EncoderParameters, elem+16, "UInt") = 1) && (NumGet(EncoderParameters, elem+20, "UInt") = 6)
        {
          p := elem + (&EncoderParameters) - pad - 4
          NumPut('UInt', Quality, NumGet(NumPut('UInt', 4, NumPut('UPtr', 1, p+0) +20), 0, 'UPtr'))
          break
        }
      }      
    }
  }

  E := DllCall("gdiplus\GdipSaveImageToFile", "ptr", pBitmap, "wstr", sOutput, "ptr", pCodecCLSID, "ptr", p)

  return E
}



Unicode4Ansi(&wString, sString)
{
  nSize := DllCall("MultiByteToWideChar", "Uint", 0, "Uint", 0, "Uint", &sString, "int", -1, "Uint", 0, "int", 0)
  wString := Buffer(nSize * 2)
  DllCall("MultiByteToWideChar", "Uint", 0, "Uint", 0, "Uint", &sString, "int", -1, "Uint", &wString, "int", nSize)
  Return	&wString
}

Gdip_Startup()
{
  Ptr := A_PtrSize ? "UPtr" : "UInt"
  
  if !DllCall("GetModuleHandle", "str", "gdiplus", Ptr)
    DllCall("LoadLibrary", "str", "gdiplus")
  ; si := Buffer(A_PtrSize = 8 ? 24 : 16, 0)
  si := Buffer(24, 0)
  numPut "uint", 1, si

  ;StrPut Chr(1), si
  ; si := Chr(1)
  ; VarSetStrCapacity(&si, A_PtrSize = 8 ? 24 : 16)
  pToken := 0
  DllCall("gdiplus\GdiplusStartup", Ptr '*', pToken, 'Ptr', si, Ptr, 0)
  return pToken
}

Gdip_Shutdown(pToken)
{
  Ptr := A_PtrSize ? "UPtr" : "UInt"
  
  DllCall("gdiplus\GdiplusShutdown", Ptr, pToken)
  if hModule := DllCall("GetModuleHandle", "str", "gdiplus", Ptr)
    DllCall("FreeLibrary", Ptr, hModule)
  return 0
}