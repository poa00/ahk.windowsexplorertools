
; Dans l'explorateur Windows : 
;	- Selectionez les fichiers à renomer
;   - lancez Win+F2 : 
;	- Donnez le préfixe à ajouter aux fichiers selectionés (un '_' sera automatiquement ajouter entre votre préfixe et le nom du fichier)

WinF2()
{
  ClipSaved := ClipboardAll()
  Send '^c'
  sleep 200
  
  
  ;Send {F4}
  ;sleep 30
  ;ControlGetText, Directorie, Edit1, A

  files := A_Clipboard
  if files = ""
    return
    


  UserInput := InputBox('Please enter a prefix.', 'Add a preix on selected files', 'w400 h130')
  if UserInput.Result != 'OK'
    return

  UserInput := UserInput.Value
  path :=
  FileList :=
  files := StrReplace(files, "`r")

    Loop Parse files, "`n"
    {
      loop Files A_LoopField
      {
          FileMove A_LoopFilePath, A_LoopFileDir '\' UserInput '_' A_LoopFileName 
      }
    }
  
  A_Clipboard := ClipSaved
  ClipSaved := "" ; Free the memory in case the clipboard was very large.
}
