
full_command_line := DllCall("GetCommandLine", "str")
if not (A_IsAdmin or RegExMatch(full_command_line, " /restart(?!\S)"))
{
    try
    {
	args := ""
	for , val in A_Args
	{
		args .= ' "' val '"'
	}
	
        if A_IsCompiled
            Run '*RunAs "' A_ScriptFullPath '" /restart "' args
        else
            Run '*RunAs "' A_AhkPath '" /restart "' A_ScriptFullPath '"' args
    }
    ExitApp
}
