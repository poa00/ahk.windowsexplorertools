
using System;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Drawing;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;

public static class ExplorerTools {
  private const string regexCutPath_str = @"((?:(?# path without """" )(?<opening>(?# 'C:/', '//', '//./<UNC/C:>', '//?/<UNC/C:>' or '%VariableWindows%')(?<UNCPrefix>[\/\\][\/\\][?.][\/\\](?:UNC[\/\\])?)?(?<montage>\b[a-zA-Z]:[\/\\])|(?:(?:\k<UNCPrefix>)|(?:[\/\\][\/\\](?!\k<montage>)))|(?:%\w+%[\/\\]?))(?:(?# directory btw // )[^\/\\<>:""|?\n\r ][^\/\\<>:""|?\n\r]*(?<![ ])[\/\\])*(?:(?#we search for fileName only if next character is pathFriendly)(?=[^\/\\<>:""'|?\n\r;, ])(?:(?#name file ? dir ?)(?:[^\/\\<>:""|?\n\r;, .](?: (?=[\w\-]))?(?:\*(?!= ))?(?!\k<montage>))+)?(?:(?#extention)\.\w+)*))|(?:(?# path quoted """" or '' )[""']\k<opening>(?=.*?[""'])(?:[^\/\\<>:'""|?\n\r]+[\/\\]?)+?[""']))";
  private static Regex regexCutPath = new Regex(regexCutPath_str, RegexOptions.Compiled);

  private const int WH_KEYBOARD_LL = 13;
  private const int WM_KEYDOWN = 0x0100;
  private const int WM_SYSKEYDOWN = 0x0104;
  // private const int WM_COPY = 0x0301;

  private static HookProc hookProc = HookCallback;
  private static IntPtr hookId = IntPtr.Zero;
  private static Keys keyCode = 0;

  [DllImport("user32.dll")]
  private static extern IntPtr CallNextHookEx(IntPtr hhk, int nCode, IntPtr wParam, IntPtr lParam);

  [DllImport("user32.dll")]
  private static extern bool UnhookWindowsHookEx(IntPtr hhk);

  [DllImport("user32.dll")]
  private static extern IntPtr SetWindowsHookEx(int idHook, HookProc lpfn, IntPtr hMod, uint dwThreadId);

  [DllImport("kernel32.dll")]
  private static extern IntPtr GetModuleHandle(string lpModuleName);

  [DllImport("user32.dll")]
  public static extern int GetAsyncKeyState(int keystate);

  [DllImport("user32.dll")]
  public static extern IntPtr GetForegroundWindow();

  [DllImport("user32.dll")]
  public static extern uint GetWindowThreadProcessId(IntPtr hWnd, UIntPtr lpdwProcessId);

  // [DllImport("user32.dll")]
  [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
  public static extern int GetClassName(IntPtr hWnd, StringBuilder lpClassName, int nMaxCount);

  [DllImport("user32.dll")]
  public static extern void keybd_event(byte bVk, byte bScan, uint dwFlags, UIntPtr dwExtraInfo);

  [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
  public static extern uint MapVirtualKey(uint uCode, uint uMapType);

  [DllImport("user32.dll")]
  public static extern int SendMessage(IntPtr hWnd, int wMsg, UIntPtr wParam, IntPtr lParam);


  public static void KeyUp(byte virtualKey)
  {
    uint sc = MapVirtualKey(virtualKey, 0);
    if (sc > 0xE000)
    {
      byte b_sc = (byte)(sc - 0xE000);
      keybd_event(virtualKey, b_sc, 2 + 1, UIntPtr.Zero);
    }
    else
      keybd_event(virtualKey, (byte)sc, 2, UIntPtr.Zero);
  }

  public static void KeyDown(byte virtualKey)
  {
    uint sc = MapVirtualKey(virtualKey, 0);
    if (sc > 0xE000)
    {
      byte b_sc = (byte)(sc - 0xE000);
      keybd_event(virtualKey, b_sc, 1, UIntPtr.Zero);
    }
    else
      keybd_event(virtualKey, (byte)sc, 0, UIntPtr.Zero);
  }

  [DllImport("user32.dll")]
  public static extern bool GetGUIThreadInfo(uint idThread, ref GUITHREADINFO pgui);
  [StructLayout(LayoutKind.Sequential)]
  public struct RECT
  {
      public int iLeft;
      public int iTop;
      public int iRight;
      public int iBottom;
  }
  [StructLayout(LayoutKind.Sequential)]
  public struct GUITHREADINFO
  {
      public int cbSize;
      public int flags;
      public IntPtr hwndActive;
      public IntPtr hwndFocus;
      public IntPtr hwndCapture;
      public IntPtr hwndMenuOwner;
      public IntPtr hwndMoveSize;
      public IntPtr hwndCaret;
      public RECT rectCaret;
  }
  

  public static void Test()
  {

  }

  public static IntPtr GetHWNDControlClassFocused() {
    var threadId = GetWindowThreadProcessId(GetForegroundWindow(), UIntPtr.Zero);
    var lpgui = new GUITHREADINFO();
    lpgui.cbSize = Marshal.SizeOf(lpgui);

    GetGUIThreadInfo(threadId, ref lpgui); 
    return lpgui.hwndFocus;
  }

  public static string GetControlClassNameFocused() {

    StringBuilder ClassName = new StringBuilder(256);
    var nRet = GetClassName(GetHWNDControlClassFocused(), ClassName, ClassName.Capacity);
    if(nRet != 0)
      return ClassName.ToString();
    else
      return "ERROR";
  }

  public static int WaitForKey() {
    hookId = SetHook(hookProc);
    Application.Run();
    UnhookWindowsHookEx(hookId);
    return (int)keyCode;
  }

  public static void Run() {
    hookId = SetHook(hookProc);
    Application.Run();
    UnhookWindowsHookEx(hookId);
  }
  
  private static HashSet<String> ExtractWinPaths(string s)
  {
    MatchCollection matches = regexCutPath.Matches(s);
    var res = new HashSet<String>();
    foreach (Match match in matches)
    {
      res.Add(match.Value.Trim('"').Trim('\'').Replace("/", "\\"));
    }
    return res;
  }

  private static IntPtr SetHook(HookProc hookProc) {
    IntPtr moduleHandle = GetModuleHandle(Process.GetCurrentProcess().MainModule.ModuleName);
    return SetWindowsHookEx(WH_KEYBOARD_LL, hookProc, moduleHandle, 0);
  }

  private delegate IntPtr HookProc(int nCode, IntPtr wParam, IntPtr lParam);

  private static IntPtr HookCallback(int nCode, IntPtr wParam, IntPtr lParam) 
  {    
    bool needForward = true;
    
    // LOG ALL INPUT ////////////////////////////
    // Console.WriteLine(((Keys)Marshal.ReadInt32(lParam)) + (wParam == (IntPtr)WM_KEYDOWN ? " DOWN" : " UP"));
    /////////////////////////////////////////////
    
    IntPtr returnVal = IntPtr.Zero;
    if (nCode >= 0 && (wParam == (IntPtr)WM_KEYDOWN || wParam == (IntPtr)WM_SYSKEYDOWN) ) 
    {
      keyCode = (Keys)Marshal.ReadInt32(lParam);
      if (keyCode == Keys.V) 
      {
        // Console.Write("V key");
        //si Ctrl+V et non Ctrl+Maj+V
        if ((GetAsyncKeyState((int)Keys.ControlKey) > 1) && !(GetAsyncKeyState((int)Keys.ShiftKey) > 1))
        {
          string controlClassName = GetControlClassNameFocused();
          bool explorerControl = controlClassName.Contains("DirectUIHWND") || controlClassName.Contains("SysListView"); // le controle focus est de type "explorer windows"
          if(Clipboard.ContainsFileDropList())
          {
            // Console.WriteLine("dropFileList");
            if(!explorerControl) //On test si le controle est de type "explorer windows". si c'est le cas, on colle directement. 
            {
              ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
              //////////////////////////////////////// Coller une liste de fichier sous forme de texte ///////////////////////////////
              ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

              var fileList = Clipboard.GetFileDropList();
              if(fileList.Count > 1)
              {
                String[] arrFile = new String[fileList.Count];
                fileList.CopyTo( arrFile, 0 );
                Clipboard.SetText("\"" + string.Join("\" \"", arrFile) + "\"");
              }
              else
                Clipboard.SetText(fileList[0]);
                
              // SendKeys.Send("+{Insert}");
              // SendKeys.Send("^v");
              // needForward = false;
              // KeyDown((byte)Keys.ControlKey);
              System.Threading.Thread.Sleep(1000);
              Clipboard.SetFileDropList(fileList);
            }
          }
          else if(Clipboard.ContainsText() && explorerControl)
          {
              ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
              /////////////////////////////// Coller une liste de path sous forme de liste de fichier ////////////////////////////////
              ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // Console.WriteLine("dropText in explorer");

            var arrFile = ExtractWinPaths(Clipboard.GetText());
            StringCollection fileList = new StringCollection();
            foreach(string path in arrFile)
            {
              fileList.Add(path);
            }
            Clipboard.SetFileDropList(fileList);

            // Console.Write(String.Join("\n", arrFile));  C:\work\clientdb.xml
          }
          
          Application.Exit();
        }
      }
      else if(keyCode == Keys.Enter)
      {

              ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
              //////////////////////////////// Ouvrir une liste de paths  ////////////////////////////////////////////////////////////
              ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        if ((GetAsyncKeyState((int)Keys.LWin) > 1) || (GetAsyncKeyState((int)Keys.RWin) > 1))
        {
          var altKey = GetAsyncKeyState((int)Keys.Menu) > 1; // Alt key
          var majKey = GetAsyncKeyState((int)Keys.ShiftKey) > 1;
          bool resetClipBoard = ! (GetAsyncKeyState((int)Keys.ControlKey) > 1);
          
          Dictionary<string, object> oldDataClipboard = new Dictionary<string, object>();

          if(resetClipBoard)
          {
            // KeyUp((byte)Keys.ControlKey);
            string[] containedFormat = Clipboard.GetDataObject().GetFormats(false);
            foreach(string format in containedFormat)
              oldDataClipboard.Add(format, Clipboard.GetData(format));
            
            // Console.WriteLine("oldDataCopied");
            

            IntPtr hwndFocusedCtrl = GetHWNDControlClassFocused();
            // Console.WriteLine(hwndFocusedCtrl);

            // Console.WriteLine("winEnter");

            // KeyUp((byte)Keys.LWin);
            // KeyUp((byte)Keys.Enter);

            var intArray = new int[1];
            intArray[0] = (int)Keys.None;
            Marshal.Copy(intArray, 0, lParam, 1);
            returnVal = CallNextHookEx(hookId, nCode, wParam, lParam);
            needForward = false;
            // Console.WriteLine("CallNextHookExed");

            // SendKeys.Flush();

//            while((GetAsyncKeyState((int)Keys.Menu) > 1) || (GetAsyncKeyState((int)Keys.LWin) > 1) || (GetAsyncKeyState((int)Keys.RWin) > 1) || (GetAsyncKeyState((int)Keys.Enter) > 1))
//              System.Threading.Thread.Sleep(100);

            bool Alt = false, LWin = false, Rwin = false, Enter = false;
            while(!(Alt && LWin && Rwin && Enter))
            {
              Alt   = Alt   || GetAsyncKeyState((int)Keys.Menu) <= 1;
              LWin  = LWin  || GetAsyncKeyState((int)Keys.LWin) <= 1;
              Rwin  = Rwin  || GetAsyncKeyState((int)Keys.RWin) <= 1;
              Enter = Enter || GetAsyncKeyState((int)Keys.Enter)<= 1;
              System.Threading.Thread.Sleep(100);
            }
            //             var intArray = new int[1];


            // KeyUp((byte)Keys.Menu);
            // System.Threading.Thread.Sleep(500);
            // while(GetAsyncKeyState((int)Keys.Menu) > 1)
              // System.Threading.Thread.Sleep(100);

            KeyDown((byte)Keys.ControlKey);
            KeyDown((byte)Keys.C);

            KeyUp((byte)Keys.C);
            KeyUp((byte)Keys.ControlKey);
            // Console.WriteLine("^C sended");

            
            System.Threading.Thread.Sleep(300);
            // Console.WriteLine("let see the fished\n" + Clipboard.GetText());

          }

          var listPath = ExtractWinPaths(Clipboard.GetText());
          
          if(altKey) // Alt key
          {
            HashSet<string> parentListPath = new HashSet<string>();
            foreach(string path in listPath)
              parentListPath.Add(System.IO.Directory.GetParent(path).FullName);

            listPath = parentListPath;
          }

          foreach(var path_ in listPath)
          {
            // Console.WriteLine("find path : " + path_);
            string path = path_;

            if(! majKey && ! System.IO.File.Exists(path))
              while(path != null && ! System.IO.Directory.Exists(path)){
                var pathInfo = System.IO.Directory.GetParent(path);
                path = pathInfo == null ? null : pathInfo.FullName;
              }

            if(path == null)
              MessageBox.Show(path_ + "\ndoes not exist\n\n");
            else
              System.Diagnostics.Process.Start(path,"");
          }

          if(resetClipBoard)
          {
            foreach(var format_data in oldDataClipboard)
              Clipboard.SetData(format_data.Key, format_data.Value);
          }
          Application.Exit();
        }
      }
    }

    if(needForward)
      return CallNextHookEx(hookId, nCode, wParam, lParam);
    else
      return returnVal;
  }

}


/*
A:\D5eploy\CE20119-AnyConnect49\SupportFiles
C:\Deploy\CE20119-AnyConnect49\Files

None       0
LButton    1
RButton    2
Cancel     3
MButton    4
XButton1   5
XButton2   6
Back       8
Tab        9
LineFeed   10
Clear      12
Enter      13
Return     13
ShiftKey   16
ControlKey 17
Menu       18
Pause      19
CapsLock   20
Capital    20
HangulMode 21
HanguelMode 21
KanaMode   21
JunjaMode  23
FinalMode  24
KanjiMode  25
HanjaMode  25
Escape     27
IMEConvert 28
IMENonconvert 29
IMEAccept  30
IMEAceept  30
IMEModeChange 31
Space      32
Prior      33
PageUp     33
PageDown   34
Next       34
End        35
Home       36
Left       37
Up         38
Right      39
Down       40
Select     41
Print      42
Execute    43
Snapshot   44
PrintScreen 44
Insert     45
Delete     46
Help       47
D0         48
D1         49
D2         50
D3         51
D4         52
D5         53
D6         54
D7         55
D8         56
D9         57
A          65
B          66
C          67
D          68
E          69
F          70
G          71
H          72
I          73
J          74
K          75
L          76
M          77
N          78
O          79
P          80
Q          81
R          82
S          83
T          84
U          85
V          86
W          87
X          88
Y          89
Z          90
LWin       91
RWin       92
Apps       93
Sleep      95
NumPad0    96
NumPad1    97
NumPad2    98
NumPad3    99
NumPad4    100
NumPad5    101
NumPad6    102
NumPad7    103
NumPad8    104
NumPad9    105
Multiply   106
Add        107
Separator  108
Subtract   109
Decimal    110
Divide     111
F1         112
F2         113
F3         114
F4         115
F5         116
F6         117
F7         118
F8         119
F9         120
F10        121
F11        122
F12        123
F13        124
F14        125
F15        126
F16        127
F17        128
F18        129
F19        130
F20        131
F21        132
F22        133
F23        134
F24        135
NumLock    144
Scroll     145
LShiftKey  160
RShiftKey  161
LControlKey 162
RControlKey 163
LMenu      164
RMenu      165
BrowserBack 166
BrowserForward 167
BrowserRefresh 168
BrowserStop 169
BrowserSearch 170
BrowserFavorites 171
BrowserHome 172
VolumeMute 173
VolumeDown 174
VolumeUp   175
MediaNextTrack 176
MediaPreviousTrack 177
MediaStop  178
MediaPlayPause 179
LaunchMail 180
SelectMedia 181
LaunchApplication1 182
LaunchApplication2 183
OemSemicolon 186
Oem1       186
Oemplus    187
Oemcomma   188
OemMinus   189
OemPeriod  190
Oem2       191
OemQuestion 191
Oem3       192
Oemtilde   192
Oem4       219
OemOpenBrackets 219
OemPipe    220
Oem5       220
OemCloseBrackets 221
Oem6       221
OemQuotes  222
Oem7       222
Oem8       223
Oem102     226
OemBackslash 226
ProcessKey 229
Packet     231
Attn       246
Crsel      247
Exsel      248
EraseEof   249
Play       250
Zoom       251
NoName     252
Pa1        253
OemClear   254
KeyCode    65535
Shift      65536
Control    131072
Alt        262144
Modifiers  -65536
*/